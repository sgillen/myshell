//====================================================================
#include "myshell.h"
int main(void){
  char input[INPUT_LENGTH];
  char* uid = getlogin();
  char cid[64]; // pretty arbitrary

  char*** pargs;
  pargs = (char***) calloc(1 , sizeof(char**)); //is this even a good way? who knows...
  
  int num_args;
  
  gethostname(cid , 64);

  //if something goes wrong let's just quit
  if(!uid){
    printf("could not get user name! exiting\n");
    return 1;
  }
  if(!cid){
    printf("could not get host name! exiting\n");
    return 1;
  }

  
  while(1==1){ //we'll exit in handle args if need be
    printf("%s@%s:%s$ ", uid, cid, get_current_dir_name());
    
    int i;
    for(i = 0; i < INPUT_LENGTH; i++){  //make sure input is clean
      input[i] = '\0';
    }
    
    fgets(input, INPUT_LENGTH, stdin);
    

    printf("got your input : %s\n" , input);

    printf("calling parse input\n");
    if(!parse_input(input, pargs, &num_args)){
      printf("invalid input, you messed something up\n");
      continue;
    }
    for(i = 0; i < num_args; i++){
      printf("*pargs[%i] : %s\n", i, (*pargs)[i]);
    }
    
    printf("handling arguments!\n");
    if(handle_args(*pargs, num_args)){
      printf("invalid argument, try something else\n");
      continue;
    }
   
  }
  
  return 0;
}


//====================================================================
//attempts to figure out what the user wanted to do
//contains the fork exec loop
int handle_args(char **args, int num_args)
{
  pid_t pid; 
  
  printf("%s" , args[0]);

  //fork exec loop
  pid = fork();
  if(pid < 0){
    printf("error! exiting\n");
    exit(1);
  }
  else if(pid == 0){
    printf("using argument : %s\n" , args[0]);
    execvp(args[0],args );
    printf("something went wrong after the first fork\n");
    fflush(stdout);
    exit(0);
  }
  else if(pid > 0){
    int status;
    wait(&status);
    
    if(status){
      printf("status is bad (status = %i), exiting\n", status);
      exit(1);
    }
    return status;  
  }
  
  free_args(args, num_args);
  
  return 1;
}
