#include "unistd.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parser.h"


#define MAX_ARGS 16
#define ARG_LENGTH 16
#define INPUT_LENGTH 128


typedef struct arg_node_{
  struct arg_node_ *left;
  struct arg_node_ *right;
  char* arg;
}arg_node;
  

int handle_args(char**, int);



