//
#include "unistd.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ARGS 16
#define ARG_LENGTH 16
#define INPUT_LENGTH 128

int parse_input(char*, char***, int*);
int free_args(char**, int);
int get_num_args(char*);
void alloc_args(char**, char*);
void contruct_arg_tree(char**);
